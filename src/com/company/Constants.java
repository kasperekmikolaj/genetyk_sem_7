package com.company;

import com.company.alg.Item;

import java.util.ArrayList;
import java.util.Arrays;

public class Constants {

  // selection parameters
  public static final int tournamentSize = 12;
  public static final double elitismPercent = 0.3;

  // crossover parameters
  public static final int numberOfCrossoverPoints = 4;

  // algorithm parameters
  public static final int populationSize = 20;
  public static final double crossoverProb = 0.15;
  public static final double mutationProb = 0.8;
  public static final int numberOfIterations = 100;

  // knapsack parameters
  //    EXAMPLE 1 -> optimal profit is 1458
  /*  public static final int optimalProfit = 1458;
  public static final int bagCapacity = 750;
  public static final ArrayList<Item> allItems =
      new ArrayList<Item>(
          Arrays.asList(
              new Item(70, 135),
              new Item(73, 139),
              new Item(77, 149),
              new Item(80, 150),
              new Item(82, 156),
              new Item(87, 163),
              new Item(90, 173),
              new Item(94, 184),
              new Item(98, 192),
              new Item(106, 201),
              new Item(110, 210),
              new Item(113, 214),
              new Item(115, 221),
              new Item(118, 229),
              new Item(120, 240)));*/

  //    EXAMPLE 2 -> optimal profit is 13549094

  public static final int optimalProfit = 13549094;
  public static final int bagCapacity = 6404180;
  public static final ArrayList<Item> allItems =
      new ArrayList<>(
          Arrays.asList(
              new Item(382745, 825594),
              new Item(799601, 1677009),
              new Item(909247, 1676628),
              new Item(729069, 1523970),
              new Item(467902, 943972),
              new Item(44328, 97426),
              new Item(34610, 69666),
              new Item(698150, 1296457),
              new Item(823460, 1679693),
              new Item(903959, 1902996),
              new Item(853665, 1844992),
              new Item(551830, 1049289),
              new Item(610856, 1252836),
              new Item(670702, 1319836),
              new Item(488960, 953277),
              new Item(951111, 2067538),
              new Item(323046, 675367),
              new Item(446298, 853655),
              new Item(931161, 1826027),
              new Item(31385, 65731),
              new Item(496951, 901489),
              new Item(264724, 577243),
              new Item(224916, 466257),
              new Item(169684, 369261)));
}
