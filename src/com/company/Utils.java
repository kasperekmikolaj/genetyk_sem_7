package com.company;

import java.util.Random;

public class Utils {

  private static final Random random = new Random();

  public static int generateRandomInt(int min, int max) {
    return random
        .ints(min, max)
        .findFirst()
        .orElseThrow(() -> new RuntimeException("random int generation error"));
  }
}
