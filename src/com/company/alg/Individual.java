package com.company.alg;

import java.util.Arrays;

public class Individual {

  private final int[] genotype;

  public Individual(int[] genotype) {
    this.genotype = genotype;
  }

  public Individual(Individual individual) {
    this.genotype = new int[individual.getGenotype().length];
    System.arraycopy(individual.getGenotype(), 0, this.genotype, 0, this.genotype.length);
  }

  public int[] getGenotype() {
    return genotype;
  }

  @Override
  public String toString() {
    return "Individual{" + "genotype=" + Arrays.toString(genotype) + '}';
  }
}
