package com.company.alg.mutation;

import com.company.alg.Individual;

public interface MutationInterface {
    void mutateIndividual(Individual individual);
}
