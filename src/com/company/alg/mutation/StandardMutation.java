package com.company.alg.mutation;

import com.company.Utils;
import com.company.alg.Individual;

public class StandardMutation implements MutationInterface {

  @Override
  public void mutateIndividual(Individual individual) {
    int mutationIndex = Utils.generateRandomInt(0, individual.getGenotype().length);
    int[] genotype = individual.getGenotype();
    genotype[mutationIndex] = genotype[mutationIndex] == 1 ? 0 : 1;
  }
}
