package com.company.alg.mutation;

import com.company.Utils;
import com.company.alg.Individual;

import java.util.Stack;

public class InversionMutation implements MutationInterface {

  @Override
  public void mutateIndividual(Individual individual) {
    int genotypeLength = individual.getGenotype().length;
    int firstInversionPoint = this.getInversionPoint(genotypeLength);
    int secondInversionPoint = this.getInversionPoint(genotypeLength);
    while (firstInversionPoint == secondInversionPoint) {
      secondInversionPoint = this.getInversionPoint(genotypeLength);
    }
    if (firstInversionPoint > secondInversionPoint) {
      int temp = firstInversionPoint;
      firstInversionPoint = secondInversionPoint;
      secondInversionPoint = temp;
    }
    Stack<Integer> temp = new Stack<>();
    for (int i = firstInversionPoint; i < secondInversionPoint; i++) {
      temp.push(individual.getGenotype()[i]);
    }
    for (int i = firstInversionPoint; i < secondInversionPoint; i++) {
      individual.getGenotype()[i] = temp.pop();
    }
  }

  private int getInversionPoint(int genotypeLength) {
    return Utils.generateRandomInt(1, genotypeLength);
  }
}
