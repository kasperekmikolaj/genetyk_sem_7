package com.company.alg.selection;

import com.company.Utils;
import com.company.alg.Individual;
import com.company.alg.KnapsackProblem;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class TournamentSelection implements SelectionInterface {

  private final int tournamentSize;
  private final KnapsackProblem problem;

  public TournamentSelection(KnapsackProblem problem, int tournamentSize) {
    this.tournamentSize = tournamentSize;
    this.problem = problem;
  }

  @Override
  public List<Individual> selectForReproduction(List<Individual> population) {
    ArrayList<Individual> selectedList = new ArrayList<>();
    while (selectedList.size() != population.size()) {
      ArrayList<Individual> tournamentMembers =
          IntStream.range(0, tournamentSize)
              .map(i -> Utils.generateRandomInt(0, population.size()))
              .mapToObj(population::get)
              .collect(Collectors.toCollection(ArrayList::new));
      int bestFitness = problem.calculateFitness(tournamentMembers.get(0));
      int bestIndex = 0;
      for (int i = 1; i < tournamentSize; i++) {
        int fit = problem.calculateFitness(tournamentMembers.get(i));
        if (fit > bestFitness) {
          bestFitness = fit;
          bestIndex = i;
        }
      }
      selectedList.add(new Individual(population.get(bestIndex)));
    }
    return selectedList;
  }
}
