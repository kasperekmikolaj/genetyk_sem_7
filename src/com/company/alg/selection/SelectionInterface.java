package com.company.alg.selection;

import com.company.alg.Individual;

import java.util.List;

public interface SelectionInterface {

    List<Individual> selectForReproduction(List<Individual> population);

}
