package com.company.alg.selection;

import com.company.alg.Individual;
import com.company.alg.KnapsackProblem;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class ElitismSelection implements SelectionInterface {

  private final double percentOfBestIndividuals;
  private final KnapsackProblem problem;

  public ElitismSelection(double percentOfBestIndividuals, KnapsackProblem problem) {
    this.percentOfBestIndividuals = percentOfBestIndividuals;
    this.problem = problem;
  }

  @Override
  public List<Individual> selectForReproduction(List<Individual> population) {
    ArrayList<Individual> selectedList = new ArrayList<>();
    List<Individual> populationCopy = new ArrayList<>(List.copyOf(population));
    populationCopy.sort(Comparator.comparingInt(problem::calculateFitness));

    int numberOfDifferentIndividuals = (int) (population.size() * percentOfBestIndividuals);
    int counter = numberOfDifferentIndividuals;
    int currentIndex = population.size() - 1;

    while (selectedList.size() != population.size()) {
      selectedList.add(new Individual(population.get(currentIndex)));
      if (counter == 0) {
        counter = numberOfDifferentIndividuals;
        currentIndex = population.size() - 1;
      } else {
        counter--;
        currentIndex--;
      }
    }
    return selectedList;
  }
}
