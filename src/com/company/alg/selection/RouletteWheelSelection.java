package com.company.alg.selection;

import com.company.alg.Individual;
import com.company.alg.KnapsackProblem;

import java.util.ArrayList;
import java.util.List;

public class RouletteWheelSelection implements SelectionInterface {

  private final KnapsackProblem problem;

    public RouletteWheelSelection(KnapsackProblem problem) {
        this.problem = problem;
    }

    @Override
  public List<Individual> selectForReproduction(List<Individual> population) {
    int totalFitness =
        population.stream().mapToInt(problem::calculateFitness).sum();
    List<Individual> selectedList = new ArrayList<>();
    List<Placement> placements = new ArrayList<>();
    double startingProb = 0;
    double finishProb =
        (double) problem.calculateFitness(population.get(0)) / (double) totalFitness;
    placements.add(new Placement(startingProb, finishProb, 0));
    for (int i = 1; i < population.size(); i++) {
      startingProb = finishProb;
      finishProb += (double) problem.calculateFitness(population.get(i)) / (double) totalFitness;
      placements.add(new Placement(startingProb, finishProb, i));
    }

    while (selectedList.size() != population.size()) {
      double rouletteResult = Math.random();
      for (Placement p : placements) {
        if (p.startingProb < rouletteResult && p.finisProb >= rouletteResult) {
          selectedList.add(population.get(p.index));
          break;
        }
      }
    }
    return selectedList;
  }

  private class Placement {

    public final double startingProb;
    public final double finisProb;
    public final int index;

    public Placement(double startingProb, double finisProb, int index) {
      this.startingProb = startingProb;
      this.finisProb = finisProb;
      this.index = index;
    }
  }
}
