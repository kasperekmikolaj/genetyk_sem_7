package com.company.alg.crossover;

import com.company.Utils;
import com.company.alg.Individual;

import java.util.ArrayList;
import java.util.List;

public class SinglePointCrossover implements CrossoverInterface {

  @Override
  public List<Individual> crossover(Individual firstParent, Individual secondParent) {
    int genotypeLength = firstParent.getGenotype().length;
    int crossoverPoint = Utils.generateRandomInt(0, genotypeLength);
    int[] firstChildGenotype = new int[genotypeLength];
    System.arraycopy(firstParent.getGenotype(), 0, firstChildGenotype, 0, genotypeLength);
    System.arraycopy(
        secondParent.getGenotype(),
        crossoverPoint,
        firstChildGenotype,
        crossoverPoint,
        genotypeLength - crossoverPoint);

    int[] secondChildGenotype = new int[genotypeLength];
    System.arraycopy(secondParent.getGenotype(), 0, secondChildGenotype, 0, genotypeLength);
    System.arraycopy(
        firstParent.getGenotype(),
        crossoverPoint,
        secondChildGenotype,
        crossoverPoint,
        genotypeLength - crossoverPoint);

    return new ArrayList<>(
            List.of(new Individual(firstChildGenotype), new Individual(secondChildGenotype)));
  }
}
