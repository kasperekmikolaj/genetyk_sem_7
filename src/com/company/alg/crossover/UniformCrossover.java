package com.company.alg.crossover;

import com.company.alg.Individual;

import java.util.ArrayList;
import java.util.List;

public class UniformCrossover implements CrossoverInterface {
  @Override
  public List<Individual> crossover(Individual firstParent, Individual secondParent) {
    int[] firstChildGenotype = new int[firstParent.getGenotype().length];
    int[] secondChildGenotype = new int[firstParent.getGenotype().length];

    for (int i = 0; i < firstChildGenotype.length; i++) {
      double randomShot = Math.random();
      if (randomShot < 0.5) {
        firstChildGenotype[i] = firstParent.getGenotype()[i];
        secondChildGenotype[i] = secondParent.getGenotype()[i];
      } else {
        firstChildGenotype[i] = secondParent.getGenotype()[i];
        secondChildGenotype[i] = firstParent.getGenotype()[i];
      }
    }
    return new ArrayList<>(
        List.of(new Individual(firstChildGenotype), new Individual(secondChildGenotype)));
  }
}
