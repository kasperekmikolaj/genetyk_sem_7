package com.company.alg.crossover;

import com.company.alg.Individual;

import java.util.List;

public interface CrossoverInterface {
    List<Individual> crossover(Individual firstParent, Individual secondParent);
}
