package com.company.alg.crossover;

import com.company.Utils;
import com.company.alg.Individual;

import java.util.*;

public class MultiplePointsCrossover implements CrossoverInterface {

  private final int numberOfCrossoverPoints;

  public MultiplePointsCrossover(int crossoverPoints) {
    this.numberOfCrossoverPoints = crossoverPoints;
  }

  @Override
  public List<Individual> crossover(Individual firstParent, Individual secondParent) {
    int genotypeLength = firstParent.getGenotype().length;
    List<Integer> crossoverPoints = new ArrayList<>();
    while (crossoverPoints.size() < this.numberOfCrossoverPoints) {
      int randomPoint = Utils.generateRandomInt(0, genotypeLength-1);
      if (!crossoverPoints.contains(randomPoint)) {
        crossoverPoints.add(randomPoint);
      }
    }
    Collections.sort(crossoverPoints);
    int[] firstChildGenotype = new int[genotypeLength];
    int[] secondChildGenotype = new int[genotypeLength];
    boolean genFromFirstParent = true;
    for (int i=0; i<genotypeLength; i++) {
      if (genFromFirstParent) {
        firstChildGenotype[i] = firstParent.getGenotype()[i];
        secondChildGenotype[i] = secondParent.getGenotype()[i];
      } else {
        firstChildGenotype[i] = secondParent.getGenotype()[i];
        secondChildGenotype[i] = firstParent.getGenotype()[i];
      }
      if (crossoverPoints.contains(i)) {
        genFromFirstParent = !genFromFirstParent;
      }
    }
    return new ArrayList<>(List.of(new Individual(firstChildGenotype), new Individual(secondChildGenotype)));
  }
}


