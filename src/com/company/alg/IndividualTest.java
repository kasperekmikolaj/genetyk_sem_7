package com.company.alg;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class IndividualTest {

  private final Individual individual1 = new Individual(new int[] {1, 1, 1, 1, 1, 1});
  private final Individual individual2 = new Individual(new int[] {0, 0, 0, 0, 0, 0});

  @org.junit.jupiter.api.Test
  void mutate() {}

  @Test
  void crossoverTest() {
    int crossoverPoint = 3;
    int[] result = new int[]{1,1,1,0,0,0};
    int[] childItems = new int[individual1.getGenotype().length];
    System.arraycopy(individual1.getGenotype(), 0, childItems, 0, individual1.getGenotype().length);
    System.arraycopy(
        individual2.getGenotype(), crossoverPoint, childItems, crossoverPoint, individual1.getGenotype().length - crossoverPoint);
    for (int i = 0; i < childItems.length; i++) {
        assertEquals(childItems[i], result[i]);
    }
  }
}
