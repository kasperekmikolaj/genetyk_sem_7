package com.company.alg;

import java.util.ArrayList;
import java.util.List;

public class KnapsackProblem {

    private final List<Item> allItems;
    private final int bagCapacity;

    public KnapsackProblem(ArrayList<Item> allItems, int bagCapacity) {
        this.bagCapacity = bagCapacity;
        this.allItems = allItems;
    }

    public int calculateFitness(Individual individual) {
        int totalWeight = 0;
        int fitness = 0;
        for (int i = 0; i < allItems.size(); i++) {
            if (individual.getGenotype()[i] == 1) {
                fitness += allItems.get(i).getValue();
                totalWeight += allItems.get(i).getWeight();
            }
        }
        return totalWeight > bagCapacity ? -1*fitness : fitness;
    }

    public List<Item> getAllItems() {
        return allItems;
    }
}
