package com.company.alg;

import com.company.Utils;
import com.company.alg.crossover.CrossoverInterface;
import com.company.alg.mutation.MutationInterface;
import com.company.alg.selection.SelectionInterface;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class GeneticAlgorithm {

  private final KnapsackProblem knapsackProblem;
  private final int numberOfIterations;
  private final int populationSize;
  private  double crossoverProb;
  private  double mutationProb;
  private final SelectionInterface selection;
  private final CrossoverInterface crossover;
  private final MutationInterface mutation;
  private List<Individual> population;

  public GeneticAlgorithm(
      KnapsackProblem knapsackProblem,
      int numberOfIterations,
      int populationSize,
      double crossoverProb,
      double mutationProb,
      SelectionInterface selection,
      CrossoverInterface crossover,
      MutationInterface mutation) {
    this.numberOfIterations = numberOfIterations;
    this.knapsackProblem = knapsackProblem;
    this.populationSize = populationSize % 2 == 0 ? populationSize : populationSize + 1;
    this.crossoverProb = crossoverProb;
    this.mutationProb = mutationProb;
    this.selection = selection;
    this.crossover = crossover;
    this.mutation = mutation;
    resetPopulation();
  }

  public void resetPopulation() {
    ArrayList<Individual> newPopulation = new ArrayList<>();
    for (int i = 0; i < populationSize; i++) {
      int[] genotype = new int[knapsackProblem.getAllItems().size()];
      for (int j = 0; j < genotype.length; j++) {
        genotype[j] = Utils.generateRandomInt(0, 2);
      }
      newPopulation.add(new Individual(genotype));
    }
    this.population = newPopulation;
  }

  public List<Individual> run(boolean elitism) {
    List<Individual> bestFromIteration = new ArrayList<>();
    for (int i = 0; i < numberOfIterations; i++) {
      makeNextGeneration(elitism);
      bestFromIteration.add(copyBestFromPopulation());
    }
    resetPopulation();
    return bestFromIteration;
  }

  private void makeNextGeneration(boolean saveBest) {
    List<Individual> newPopulation = new ArrayList<>();
    if (saveBest) {
      List<Individual> populationCopy = new ArrayList<>(List.copyOf(population));
      populationCopy.sort(Comparator.comparingInt(knapsackProblem::calculateFitness));
      newPopulation.add(new Individual(populationCopy.get((populationSize - 1))));
      newPopulation.add(new Individual(populationCopy.get((populationSize - 2))));
    }
    List<Individual> selectedForReproduction = selection.selectForReproduction(this.population);
    int i = 0;
    while (newPopulation.size() != populationSize) {
      Individual firstParent = selectedForReproduction.get(i);
      Individual secondParent = selectedForReproduction.get(i + 1);
      i += 2;
      Individual firstChild;
      Individual secondChild;
      double individualCrossoverProb = Math.random();
      double firstChildMutationProb = Math.random();
      double secondChildMutationProb = Math.random();
      if (individualCrossoverProb <= crossoverProb) {
        List<Individual> children = crossover.crossover(firstParent, secondParent);
        firstChild = children.get(0);
        secondChild = children.get(1);
      } else {
        firstChild = firstParent;
        secondChild = secondParent;
      }

      if (firstChildMutationProb <= mutationProb) this.mutation.mutateIndividual(firstChild);
      if (secondChildMutationProb <= mutationProb) this.mutation.mutateIndividual(secondChild);

      newPopulation.add(firstChild);
      newPopulation.add(secondChild);
    }
    this.population = newPopulation;
  }

  private Individual copyBestFromPopulation() {
    int bestFitAfterIteration = knapsackProblem.calculateFitness(this.population.get(0));
    Individual bestIndividualAfterIteration = this.population.get(0);
    for (int j = 1; j < populationSize; j++) {
      Individual current = population.get(j);
      int currentFit = knapsackProblem.calculateFitness(current);
      if (currentFit > bestFitAfterIteration) {
        bestFitAfterIteration = currentFit;
        bestIndividualAfterIteration = current;
      }
    }
    return new Individual(bestIndividualAfterIteration);
  }

  public void setCrossoverProb(double crossoverProb) {
    this.crossoverProb = crossoverProb;
  }

  public void setMutationProb(double mutationProb) {
    this.mutationProb = mutationProb;
  }
}
