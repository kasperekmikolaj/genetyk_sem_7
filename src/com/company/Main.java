package com.company;

import com.company.alg.GeneticAlgorithm;
import com.company.alg.Individual;
import com.company.alg.KnapsackProblem;
import com.company.alg.crossover.CrossoverInterface;
import com.company.alg.crossover.MultiplePointsCrossover;
import com.company.alg.crossover.SinglePointCrossover;
import com.company.alg.crossover.UniformCrossover;
import com.company.alg.mutation.InversionMutation;
import com.company.alg.mutation.MutationInterface;
import com.company.alg.mutation.StandardMutation;
import com.company.alg.selection.ElitismSelection;
import com.company.alg.selection.RouletteWheelSelection;
import com.company.alg.selection.SelectionInterface;
import com.company.alg.selection.TournamentSelection;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.List;

import static com.company.Constants.*;

public class Main {

  static DecimalFormat decimalFormat = new DecimalFormat("#.##");
  static KnapsackProblem problemData = new KnapsackProblem(allItems, bagCapacity);

  static SelectionInterface tournamentSelection =
      new TournamentSelection(problemData, tournamentSize);
  static SelectionInterface elitismSelection = new ElitismSelection(elitismPercent, problemData);
  static SelectionInterface rouletteWheelSelection = new RouletteWheelSelection(problemData);

  static CrossoverInterface singlePointCrossover = new SinglePointCrossover();
  static CrossoverInterface uniformCrossover = new UniformCrossover();
  static CrossoverInterface multiplePointsCrossover =
      new MultiplePointsCrossover(numberOfCrossoverPoints);

  static MutationInterface standardMutation = new StandardMutation();
  static MutationInterface inversionMutation = new InversionMutation();

  public static void main(String[] args) {

    GeneticAlgorithm alg =
        new GeneticAlgorithm(
            problemData,
            numberOfIterations,
            populationSize,
            crossoverProb,
            mutationProb,
            tournamentSelection,
            singlePointCrossover,
            standardMutation);

    // TESTING
    /*
            System.out.println("Best possible score is " + optimalProfit);
            System.out.println("Achieved scores:");
            for (int i = 0; i < 1; i++) {
              alg.resetPopulation();
              List<Individual> bestIndividuals = alg.run(true);
              for (Individual individual : bestIndividuals) {
                int result = problemData.calculateFitness(individual);
                if (result == optimalProfit) {
                  System.out.println(result + " <- best possible score");
                } else {
                  System.out.println(result);
                }
              }
            }
    */

    // EXERCISE 1
    /*
    try (BufferedWriter writer = new BufferedWriter(new FileWriter(new File("exercise_1.csv")))) {

      String firstLine =
          "crossoverProb;mutationProb;result_1;result_2;result_3;result_4;result_5;result_6;result_7;result_8;result_9;result_10;average";
      writer.write(firstLine);
      writer.newLine();

      for (double mutationProb = 0.05; mutationProb < 1; mutationProb += 0.05) {
        double crossoverProb = 0.5;
        StringBuilder newLine =
            new StringBuilder(
                decimalFormat.format(crossoverProb)
                    + ";"
                    + decimalFormat.format(mutationProb)
                    + ";");
        int[] scores = new int[10];
        for (int i = 0; i < 10; i++) {
          alg.setCrossoverProb(crossoverProb);
          alg.setMutationProb(mutationProb);
          List<Individual> results = alg.run(true);
          int lastScore = problemData.calculateFitness(results.get(results.size() - 1));
          scores[i] = lastScore;
          newLine.append(lastScore).append(";");
        }
        newLine.append(
            decimalFormat.format(
                Arrays.stream(scores).average().orElseThrow(() -> new Exception("no average"))));
        writer.write(newLine.toString());
        writer.newLine();
      }

      writer.newLine();
      writer.write(firstLine);
      writer.newLine();

      for (double crossoverProb = 0.05; crossoverProb < 1; crossoverProb += 0.05) {
        double mutationProb = 0.2;
        StringBuilder newLine =
            new StringBuilder(
                decimalFormat.format(crossoverProb)
                    + ";"
                    + decimalFormat.format(mutationProb)
                    + ";");
        int[] scores = new int[10];
        for (int i = 0; i < 10; i++) {
          alg.setCrossoverProb(crossoverProb);
          alg.setMutationProb(mutationProb);
          List<Individual> results = alg.run(true);
          int lastScore = problemData.calculateFitness(results.get(results.size() - 1));
          scores[i] = lastScore;
          newLine.append(lastScore).append(";");
        }
        newLine.append(
            decimalFormat.format(
                Arrays.stream(scores).average().orElseThrow(() -> new Exception("no average"))));
        writer.write(newLine.toString());
        writer.newLine();
      }

    } catch (Exception e) {
      e.printStackTrace();
    }*/

    // EXERCISE 2
    int iterationsForEachTest = 100;
    try (BufferedWriter writer = new BufferedWriter(new FileWriter("exercise_2.csv"))) {
      String firstLine = "selection;crossover;mutation;result";
      writer.write(firstLine);
      writer.newLine();

      alg =
          new GeneticAlgorithm(
              problemData,
              numberOfIterations,
              populationSize,
              crossoverProb,
              mutationProb,
              tournamentSelection,
              singlePointCrossover,
              standardMutation);
      String newLine = "tournament;singlePoint;standard;";
      runAndSave(alg, newLine, iterationsForEachTest, writer);
      writer.newLine();

      alg =
          new GeneticAlgorithm(
              problemData,
              numberOfIterations,
              populationSize,
              crossoverProb,
              mutationProb,
              rouletteWheelSelection,
              singlePointCrossover,
              standardMutation);
      writer.newLine();
      newLine = "rouletteWheel;singlePoint;standard;";
      runAndSave(alg, newLine, iterationsForEachTest, writer);
      writer.newLine();

      alg =
          new GeneticAlgorithm(
              problemData,
              numberOfIterations,
              populationSize,
              crossoverProb,
              mutationProb,
              elitismSelection,
              singlePointCrossover,
              standardMutation);
      writer.newLine();
      newLine = "elitism;singlePoint;standard;";
      runAndSave(alg, newLine, iterationsForEachTest, writer);
      writer.newLine();

      alg =
          new GeneticAlgorithm(
              problemData,
              numberOfIterations,
              populationSize,
              crossoverProb,
              mutationProb,
              tournamentSelection,
              singlePointCrossover,
              inversionMutation);
      newLine = "tournament;singlePoint;inversion;";
      runAndSave(alg, newLine, iterationsForEachTest, writer);
      writer.newLine();

      alg =
          new GeneticAlgorithm(
              problemData,
              numberOfIterations,
              populationSize,
              crossoverProb,
              mutationProb,
              tournamentSelection,
              uniformCrossover,
              standardMutation);
      newLine = "tournament;uniform;standard;";
      runAndSave(alg, newLine, iterationsForEachTest, writer);
      writer.newLine();

      alg =
          new GeneticAlgorithm(
              problemData,
              numberOfIterations,
              populationSize,
              crossoverProb,
              mutationProb,
              tournamentSelection,
              multiplePointsCrossover,
              standardMutation);
      newLine = "tournament;4-pointCrossover;standard;";
      runAndSave(alg, newLine, iterationsForEachTest, writer);
      writer.newLine();

    } catch (Exception e) {
      e.printStackTrace();
    }
  } // END OF MAIN

  private static void runAndSave(
      GeneticAlgorithm alg, String lineBegin, int numberOfTimes, BufferedWriter writer)
      throws IOException {
    for (int i = 0; i < numberOfTimes; i++) {
      List<Individual> results = alg.run(true);
      int finalResult = problemData.calculateFitness(results.get(results.size() - 1));
      writer.write(lineBegin + finalResult);
      writer.newLine();
    }
  }
} // END OF CLASS
